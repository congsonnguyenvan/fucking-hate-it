import { createConnection } from 'typeorm';

createConnection({
  type: 'mysql',
  host: 'localhost',
  port: 3306,
  username: 'root',
  password: 'Chucthanhlam1907',
  database: 'wordle_game',
  entities: ['src/**/*.entity.{ts,js}'],
  synchronize: true,
}).then(() => {
  console.log('Connected to database');
}).catch((error) => {
  console.log('Error connecting to database', error);
});
