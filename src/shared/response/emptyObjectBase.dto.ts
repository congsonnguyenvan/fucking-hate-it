import { EmptyObject } from './emptyObject.dto';
import { ApiProperty, ApiResponseProperty } from '@nestjs/swagger';

export class EmptyObjectBase {
  @ApiResponseProperty({
    type: EmptyObject,
  })
  data: EmptyObject;
}
