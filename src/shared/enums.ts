export enum HotWalletStatus {
    COMPLETED = 'completed',
    FAILED = 'failed',
}

export enum Currency {
    BIV = 'biv',
    WBIV = 'wbiv',
    ETH = 'eth',
}


export enum Role {
    User = 1,
    Admin = 2,
    SuperAdmin = 3,
  }

export enum StakeLogType {
    STAKE = 'stake',
    UNSTAKE = 'unstake',
    CLAIM = 'claim',
}

export enum HistoryTag {
    USER = 'user',
    NFT = 'nft',
    STAKE = 'stake',
    NFT_ORDER = 'nft_order',
    WDT_ORDER = 'wdt_order',
    GAME = 'game',

}

export enum CollectionType {
    ALL = 'all',
    NFT = 'nft',
    WDT = 'wdt',
    COIN = 'coin',
    LOOT = 'loot',
    KEY = 'key',
    LOOTANDKEY = 'lootAndKey',
}

export enum NftOrderStatus {
    NEW = 'new',
    LISTED = 'listed',
    CANCELLED = 'cancelled',
    REQUESTED = 'requested',
    TRANSFERRING = 'transferring',
    SUCCESSFUL = 'successful'
}

export enum StakeStatus {
    ACTIVE = 'active',
    STAKING = 'staking',
    INACTIVE = 'inactive',
}

export enum NftType {
    BADGE = 'badge',
    LETTER = 'letter',
    LOOT_BOX = 'loot_box',
    LOOT_BOX_KEY = 'loot_box_key',
}

export enum Char {
    A = 'a',
    B = 'b',
    C = 'c',
    D = 'd',
    E = 'e',
    F = 'f',
    G = 'g',
    H = 'h',
    I = 'i',
    J = 'j',
    K = 'k',
    L = 'l',
    M = 'm',
    N = 'n',
    O = 'o',
    P = 'p',
    Q = 'q',
    R = 'r',
    S = 's',
    T = 't',
    U = 'u',
    V = 'v',
    W = 'w',
    X = 'x',
    Y = 'y',
    Z = 'z'
}

export enum NftStatus {
    LISTED = "listed",
    PROCESSING = "processing",
    SALE = "sale",
    STAKED = "staked",
    CENSORED = "censored",
    LOOT_BOX = "loot_box",
}

export enum StakeAction {
    STAKE = 'stake',
    UNSTAKE = 'unstake',
}

export enum Ranks {
    IRON = 'iron',
    BRONZE = 'bronze',
    SILVER = 'silver',
    GOLD = 'gold',
    PLATINUM = 'platinum',
    DIAMOND = 'diamond',
    MASTER = 'master',
    TITANIUM = 'titanium',
    GRANDMASTER = 'grandmaster',
}

export enum NftConfigStatus {
    ACTIVE = 'active',
    INACTIVE = 'inactive',
}

export enum LoginMethod {
    PASS = 'pass',
    EMAIL_CODE = 'email-code',
    SMS_CODE = 'sms-code'
}

export enum WEBHOOK_TYPE {
    TRANSFER = 'transfer',
    TXCHANGED = 'tx_changed',
    COMMON = 'common',
}

export enum TransactionType {
    DEPOSIT = 'deposit',
    WITHDRAWAL_NORMAL = 'withdrawal_normal',
    WITHDRAWAL_COLD = 'withdrawal_cold',
    SEED = 'seed',
    COLLECT = 'collect',
    WITHDRAWAL_COLLECT = 'withdrawal_collect',
}

export enum WithdrawalStatus {
    INVALID = 'invalid',
    UNSIGNED = 'unsigned',
    SIGNING = 'signing',
    SIGNED = 'signed',
    SENT = 'sent',
    COMPLETED = 'completed',
    FAILED = 'failed',
}

export enum PendingWithdrawalStatus {
    UNSIGNED = 'unsigned',
    SIGNING = 'signing',
    SIGNED = 'signed',
    SENT = 'sent',
}

export enum P2POrderFiat {
    DOGE = 'DOGE',
    USDT = 'USDT',
    SHIBA = 'SHIBA',
}

export enum referralLevel {
    BASIC = 1,
    TOWN = 2,
    CITY = 3,
    STATE = 4,
    COUNTRY = 5,
}

export enum WithdrawOutType {
    WITHDRAW_OUT_COLD_SUFFIX = '_cold_withdrawal',
    WITHDRAW_OUT_NORMAL = 'normal',
    EXPLICIT_FROM_HOT_WALLET = 'explicit_from_hot_wallet',
    EXPLICIT_FROM_DEPOSIT_ADDRESS = 'explicit_from_deposit_address',
    AUTO_COLLECTED_FROM_DEPOSIT_ADDRESS = 'auto_collected_from_deposit_address',
}

export enum WalletEvent {
    CREATED = 'created',
    DEPOSIT = 'deposit',
    WITHDRAW_REQUEST = 'withdraw_request',
    WITHDRAW_COMPLETED = 'withdraw_completed',
    WITHDRAW_FAILED = 'withdraw_failed',
    WITHDRAW_FEE = 'withdraw_fee',
    WITHDRAW_ACCEPTED = 'withdraw_accepted',
    WITHDRAW_DECLINED = 'withdraw_declined',
    COLLECT_FEE = 'collect_fee',
    COLLECT_AMOUNT = 'collect_amount',
    COLLECTED_FAIL = 'collected_fail',
    COLLECTED = 'collected',
    SEEDED_FAIL = 'seeded_fail',
    SEEDED = 'seeded',
    SEED_FEE = 'seed_fee',
    SEED_AMOUNT = 'seed_amount',
}

export enum CollectStatus {
    UNCOLLECTED = 'uncollected',
    COLLECTING_FORWARDING = 'forwarding',
    COLLECTING = 'collecting',
    COLLECT_SIGNED = 'collect_signed',
    COLLECT_SENT = 'collect_sent',
    COLLECTED = 'collected',
    NOTCOLLECT = 'notcollect',
    SEED_REQUESTED = 'seed_requested',
    SEEDING = 'seeding',
    SEED_SIGNED = 'seed_signed',
    SEED_SENT = 'seed_sent',
    SEEDED = 'seeded',
}

export enum HotWalletType {
    NORMAL = 'normal',
    SEED = 'seed',
}

export enum SortBy {
    UPDATED_AT = 'updatedAt',
    AMOUNT = 'amount',
}

export enum SortType {
    SortTypeASC = 'asc',
    SortTypeDESC = 'desc',
}

export enum FolderImageS3 {
    KYC = 'kyc',
    USERS = 'users',
    P2P = 'p2p-trading'
}

export enum KYCStatus {
    PENDING = 'pending',
    APPROVED = 'approved',
    REJECTED = 'rejected',
}

export enum OrderStatus {
    ACTIVE = 'active',
    INACTIVE = 'inactive',
}

export enum MatchType {
    CLASSIC = 'classic',
    BATTLE = 'battle',
    ONE_VS_ONE = '1v1',
    TWO_VS_TWO = '2v2',
    THREE_VS_THREE = '3v3',
    FOUR_VS_FOUR = '4v4',
    FIVE_VS_FIVE = '5v5',
    REJECT = 'reject',
}

export enum TeamType {
    ONE = 1,
    TWO = 2,
}

export enum MatchStatus {
    PENDING = 'pending',
    PLAYING = 'playing',
    FINISHED = 'finished',
}

export enum PlayerType {
    OWNER = 'owner',
    OTHER = 'other',
}

export enum MatchRewardType {
    WDT = 'wdt',
    LOOT_BOX = 'loot_box',
    LOOT_BOX_KEY = 'loot_box_key',
    AVATAR = 'avatar',
}

export enum RankStatus {
    SAME = 'same',
    UP = 'up',
    DOWN = 'down',
}

export enum LootBoxStatus {
    LISTED = "listed",
    SALE = "sale",
    OPENED = "opened",
}

export enum LootBoxRewardType {
    NFT = "nft",
    NON_STAKING_NFT = "non_staking_nft",
    RIGHT_TO_BUY_NFT = "right_to_buy_nft",
    WDT = "wdt",
    AVATAR = "avatar",
    LOOT_BOX_KEY = "loot_box_key",
}

export enum LootBoxOpenStatus {
    TRANSFERRED = "transferred",
    PROCESSING = "processing",
}

export enum RightToBuyNftStatus {
    ACTIVATED = "activated",
    DEACTIVATED = "deactivated",
    USED = "used",
}
// SOCKET
export enum GameEvents {
    JOIN_ROOM = "join_room",
    GAME_REWARD = "game_reward",
    MATCH_CREATED = "match_created",
    START_GAME = "start_game",
    GUESS_WORD = "guess_word",
    SHARE_TWEET = "share_tweet",
}

export enum P2PTrading {
    TRANSACTION_WDT_CREATED = "transaction_wdt_created",
    TRANSACTION_WDT_CANCELED = "transaction_wdt_canceled",
    TRANSACTION_WDT_CONFIRM_DEPOSITED = "transaction_wdt_confirm_deposited",
    TRANSACTION_WDT_CONFIRM_RECEIVED = "transaction_wdt_confirm_received",
    TRANSACTION_NFT_CREATED = "transaction_nft_created",
    TRANSACTION_NFT_ONCHAIN_SUCCESSFULLY = "transaction_nft_onchain_successfully",
}

export enum OrderType {
    SELL = "sell",
    BUY = "buy"
}

export enum WalletTypes {
    MASTER_WALLET = "master_wallet",
    USER_WALLET = "user_wallet"
}

export enum ReturnMessageAdmin {
    DELETE_AVATAR = "Deleted avatar",
    UPDATE_TOKEN_CONFIG = "Update token config successfully",
    ADD_TOKEN_CONFIG = "Add token config successfully",
}

export enum AdminEvents {
    ADMIN_INSUFFICIENT_BALANCE = "admin_insufficient_balance",
}

export enum ReturnMessageGame {
    VERIFYING_REQUEST = "Verifying request create"
}

export enum ReturnMessageP2P {
    UPDATE_MIN_AMOUNT = "Update min amount successful",
    DELETE_ORDER = "Deleted WDT Order",
}

export enum UserBalanceLogType {
    NFT_TRANSFER = "nft_transfer",
    WDT_TRANSFER = "wdt_transfer",
    WDT_SWAP = "wdt_swap",
    BETTING = "betting",
    WINNING = "winning",
    DIVIDENDS = "dividends",
    STAKING_REWARD = "staking_reward",
    REFERRAL_BONUS = "referral_bonus"
}

export enum UserBalanceType {
    GAMING = 'gaming',
    TRADING = 'trading',
}