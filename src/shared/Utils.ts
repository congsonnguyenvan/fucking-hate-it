import * as CryptoJS from 'crypto-js';
import { SelectQueryBuilder } from 'typeorm';
import { fromBuffer } from "file-type";
import { Causes } from "../config/exception/causes";
import { Ranks } from "./enums";


export function nowInMillis(): number {
  return Date.now();
}

// Alias for nowInMillis
export function now(): number {
  return nowInMillis();
}

export function nowInSeconds(): number {
  return (nowInMillis() / 1000) | 0;
}

export function addHttps(url: string) {
  if (!/^(?:f|ht)tps?\:\/\//.test(url)) {
    url = 'https://' + url;
  }
  return url;
}


export function union<T>(
  initialQueryBuilder: SelectQueryBuilder<T>,
  ...queries: SelectQueryBuilder<T>[]
): UnionParameters {
  const stringfyQuery = (
    query: UnionParameters,
    queryBuilder: SelectQueryBuilder<T>,
    index: number
  ): UnionParameters => {
    const sql = `(${queryBuilder.getQuery()})`;

    // If first recursion
    if (!index) {
      return {
        getQuery: () => sql,
        getParameters: () => queryBuilder.getParameters(),
      };
    }

    // If not first recursion
    return {
      getQuery: () => `${query.getQuery()} UNION ${sql}`,
      getParameters: () => ({
        ...queryBuilder.getParameters(),
        ...query.getParameters(),
      }),
    };
  };

  const { getQuery, getParameters } = queries.reduce(stringfyQuery, {
    getQuery: initialQueryBuilder.getQuery,
    getParameters: initialQueryBuilder.getParameters,
  });

  return {
    // Wrap final sql in parantheses to allow for table alias
    getQuery: () => `(${getQuery()})`,
    getParameters,
  };
}

interface UnionParameters {
  getQuery: () => string;
  getParameters: () => any;
}

export function snakeToCamel(obj: any): any {
  if (Array.isArray(obj)) {
    return obj.map((val) => snakeToCamel(val));
  } else if (obj !== null && typeof obj === "object") {
    return Object.keys(obj).reduce((result, key) => {
      const newKey = key.replace(/_([a-z])/g, (m, p1) => p1.toUpperCase());
      result[newKey] = snakeToCamel(obj[key]);
      return result;
    }, {});
  }
  return obj;
}

export function encrypt(data: string) {
  return CryptoJS.AES.encrypt(data, process.env.CRYPTO_KEY || 'varmeta@123').toString();
}

export function decrypt(data: string) {
  return CryptoJS.AES.decrypt(data, process.env.CRYPTO_KEY || 'varmeta@123').toString(CryptoJS.enc.Utf8);
}

export function decryptToJSON(data: string) {
  return JSON.parse(CryptoJS.AES.decrypt(data, process.env.CRYPTO_KEY || 'varmeta@123').toString(CryptoJS.enc.Utf8));
}



export function convertToString(value: any) {
  return (typeof value === 'string') ? value : '';
}

export function convertToObject(value: any) {
  return (typeof value === 'object') ? value : {};
}

export function existValueInEnum(type: any, value: any): boolean {
  return (
    Object.keys(type)
      .filter((k) => isNaN(Number(k)))
      .filter((k) => type[k] === value).length > 0
  );
}

export async function checkImage(file) {
  console.log("file: ", file);
  const listType = [
    "JPG",
    "JPEG",
    "PNG",
    "GIF",
    "SVG",
    "MP4",
    "WEBM",
    "MP3",
    "MPEG",
    "WAV",
    "OGG",
    "GLB",
    "GLTF",
    "SVG+XML",
    "OCTET-STREAM",
    "STL",
    "3MF",
    "3DS",
    "MAX",
    "OBJ",
    "COLLADA",
    "VRML",
    "X3D",
    "STP",
    "FBX",
    "GLTF-BINARY",
  ];
  console.log("file.mimetype", file.mimetype);
  const imgType = file.mimetype.split("/")[1].toUpperCase();
  const imgSize = file.size;
  if (imgSize > 100 * 1000 * 1000) throw Causes.FILE_SIZE_OVER;
  if (!Buffer.isBuffer(file.buffer)) throw Causes.FILE_TYPE_INVALID;
  const checkFileType = await fromBuffer(file.buffer);
  console.log('checkFileType: ', checkFileType)
  if (
    !checkFileType ||
    !(listType.includes(checkFileType.ext.toUpperCase()) || checkFileType.mime.includes('image/')) ||
    !listType.includes(imgType)
  ) throw Causes.FILE_TYPE_INVALID;

  return true;
}


export function isJsonString(str) {
  try {
    JSON.parse(str);
  } catch (e) {
    return false;
  }
  return true;
}

export function randomize(bigProbability: number, smallProbability: number) {
  const r = Math.random();

  if (bigProbability + smallProbability != 1) {
    throw new Error('bigProbability + smallProbability must be 1');
  }

  if (r <= smallProbability) {
    return 1;
  } else {
    return 2;
  }
}

export function mapRankLevelToText(rankLevel: number) {
  switch (rankLevel) {
    case 1:
      return Ranks.IRON;
    case 2:
      return Ranks.BRONZE;
    case 3:
      return Ranks.SILVER;
    case 4:
      return Ranks.GOLD;
    case 5:
      return Ranks.PLATINUM;
    case 6:
      return Ranks.DIAMOND;
    case 7:
      return Ranks.MASTER;
    case 8:
      return Ranks.TITANIUM;
    default:
      return Ranks.GRANDMASTER;
  }
}