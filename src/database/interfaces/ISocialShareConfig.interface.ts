export interface ISocialShareConfig {
    matchType?: string;
    startTime?: number;
    endTime?: number;
}