export interface IMatchBetConfig {
    matchType?: string;
    amountPerMatch?: number;
    rankLevel?: number;
}