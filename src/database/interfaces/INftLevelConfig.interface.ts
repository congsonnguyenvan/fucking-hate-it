import { NftConfigStatus, Ranks } from "../../shared/enums";

export interface INftLevelConfig {
    rank: Ranks;
    nftPrice: number;
    earningRate: number;
    weight: number;
    purchaseLimit: number;
    cycle: number;
    status?: NftConfigStatus;
    minGrade: number;
    createdBy: number;
    updatedBy: number;
}