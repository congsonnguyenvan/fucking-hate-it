export interface IRankConfig {
    id: number;
    level: number;
    title: string;
    description: string;
    rankThreshold: number;
}