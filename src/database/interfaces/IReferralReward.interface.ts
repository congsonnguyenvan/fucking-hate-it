export interface IReferralReward {
    referralTitle: string,
    referralLevel: number,
    minReferralUser: number,
    minPersonalNFT: number,
    minGroupNFT: number,
    minReferralGroup: number,
    dailyReward: number,
}