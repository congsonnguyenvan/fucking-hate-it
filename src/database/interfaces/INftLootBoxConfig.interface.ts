export interface INftLootBoxConfigInterface {
    rankLevel: number;
    nftLevel: number;
    nftPrice: number;
    probability: number;
    createdAt: number;
    updatedAt: number;
}