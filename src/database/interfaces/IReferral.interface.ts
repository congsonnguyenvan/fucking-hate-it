export interface IReferral {
    userId: number,
    referralCode: string,
    referralLevel: number,
    totalReferralUser: number,
    totalGroupNFT: number,
    totalReferralGroup: number,
    inviter: number,
}