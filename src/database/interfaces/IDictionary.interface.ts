export interface IDictionaryInterface {
    id: number;
    word: string;
    meanings: string;
    createdAt: number;
    updatedAt: number;
}