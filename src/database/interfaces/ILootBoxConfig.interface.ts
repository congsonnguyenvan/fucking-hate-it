export interface ILootBoxConfigInterface {
    rankLevel: number;
    rewardType: string;
    probability: number;
    createdAt: number;
    updatedAt: number;
}