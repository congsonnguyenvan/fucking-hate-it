export interface IUser {
    username: string,
    password: string,
    email: string,
    phone:  string,
    avatarUrl: string,
    wallet: string,
}