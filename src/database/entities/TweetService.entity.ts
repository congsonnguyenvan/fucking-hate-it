import { NftOrderStatus } from '../../shared/enums';
import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';
import { truncate } from 'fs';

@Entity('tweet_service')
export class TweetService {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;
    
    @Column({ name: 'user_id', type: 'int', nullable: false })
    userId: number;

    @Column({ name: 'match_id', type: 'int', nullable: false })
    matchId: number;

    @Column({ name: 'match_code', type: 'varchar', nullable: false })
    matchCode: string;

    @Column({ name: 'tweet_id', type: 'varchar', nullable: false })
    tweetId: string;

    //1 is done, 0 is haven't done
    @Column({ name: 'status', type: 'tinyint', nullable: false, default: 0 })
    status: number;

    //1 is done, 0 is haven't done
    @Column({ name: 'instruction', type: 'varchar',length: 255, nullable: true })
    instruction: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    public createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    public updatedAt: number;

    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
