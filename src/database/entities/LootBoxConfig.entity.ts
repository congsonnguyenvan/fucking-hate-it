import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {LootBoxRewardType} from "../../shared/enums";
import {nowInMillis} from "../../shared/Utils";

@Entity("loot_box_config")
export class LootBoxConfig  {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'rank_level', type: 'int', nullable: false, default: 0 })
    rankLevel: number;

    @Column({ name: 'reward_type', type: 'enum', enum: LootBoxRewardType, nullable: false})
    rewardType: string;

    @Column({ name: 'probability', type: 'decimal', precision:16, scale: 6, nullable: false })
    probability: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}