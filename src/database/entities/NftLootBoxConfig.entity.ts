import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {LootBoxRewardType} from "../../shared/enums";
import {nowInMillis} from "../../shared/Utils";
import {INftLootBoxConfigInterface} from "../interfaces/INftLootBoxConfig.interface";

@Entity("nft_loot_box_config")
export class NftLootBoxConfig implements INftLootBoxConfigInterface{
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'rank_level', type: 'int', nullable: false, default: 0 })
    rankLevel: number;

    @Column({ name: 'nft_level', type: 'int', nullable: false, default: 0 })
    nftLevel: number;

    @Column({ name: 'nft_price', type: 'decimal', precision:16, scale: 6, nullable: false })
    nftPrice: number;

    @Column({ name: 'probability', type: 'decimal', precision:16, scale: 6, nullable: false })
    probability: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}