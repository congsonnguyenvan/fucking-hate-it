import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";
import {RankStatus} from "../../shared/enums";

@Entity('rank')
@Index('user_id', ['userId'], { unique: false })
@Index('month', ['month'], { unique: false })
@Index('year', ['year'], { unique: false })
@Index('created_at', ['createdAt'], { unique: false })
@Index('updated_at', ['updatedAt'], { unique: false })
export class Rank {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'user_id', type: 'int', nullable: false })
    userId: number;

    @Column({ name: 'level', type: 'int', default: 1, nullable: false })
    level: number;

    @Column({ name: 'current_mmr', type: 'int', default: 0, nullable: false })
    currentMmr: number;

    @Column({ name: 'total_mmr', type: 'int', default: 0, nullable: false })
    totalMmr: number;

    @Column({ name: 'month', type: 'tinyint', nullable: false })
    month: number;

    @Column({ name: 'year', type: 'smallint', nullable: false })
    year: number;

    @Column({ name: 'wins', type: 'int', nullable: false, default: 0})
    wins: number;

    @Column({ name: 'lost', type: 'int', nullable: false, default: 0})
    lost: number;

    @Column({ name: 'status', type: 'varchar', length: 20, nullable: true })
    status: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}