import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('metadata')
export class Metadata {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'name', type: 'varchar', length: 255, nullable: true })
    name: string;

    @Column({ name: 'metadata_url', type: 'varchar', length: 500, nullable: false })
    metadataUrl: string;

    // letter
    // badge
    // loot_box
    // loot_box_key
    @Column({ name: 'type', type:'varchar', length: 50, nullable: true })
    type: string;

    @Column({ name: 'rank', type: 'varchar', length: 50, nullable: true })
    rank: string;

    @Column({ name: 'char', type: 'varchar', length: 50, nullable: true })
    char: string;

    @Column({ name: 'raw_data', type: 'varchar', length: 5000, nullable: true })
    rawData: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}
