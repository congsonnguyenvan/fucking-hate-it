import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('transaction_nft_onchain')
export class TransactionNFTOnchain {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int', unsigned: true })
  public id: number;

  @Column({ name: 'fee', type: 'int', nullable: true })
  public fee: number;

  @Column({ name: 'instruction', type: 'varchar', nullable: true })
  public type: string;

  @Column('varchar', { name: 'account', nullable: true })
  public account: string;

  @Column('varchar', { name: 'amount', nullable: true })
  public amount: string;

  @Column('varchar', { name: 'mintAddress', nullable: true })
  public mintAddress: number;

  @Column('varchar', { name: 'mintAuthority', nullable: true })
  public mintAuthority: number;

  @Column({ name: 'signature', type: 'varchar', length: 250, nullable: true })
  public signature: string;

  //error if transaction failed, null if succeeded
  @Column('varchar', { length: 5000, name: 'err', nullable: true })
  public err: string | null;

  @Column({ name: 'created_at', type: 'bigint', nullable: true })
  public createdAt: number;

  @Column({ name: 'updated_at', type: 'bigint', nullable: true })
  public updatedAt: number;



  
}
