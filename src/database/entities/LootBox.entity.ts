import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {LootBoxRewardType, LootBoxStatus} from "../../shared/enums";
import {nowInMillis} from "../../shared/Utils";

@Entity("loot_box")
@Index("match_id", ["matchId"], { unique: false })
@Index("owner_id", ["ownerId"], { unique: false })
@Index("creator_id", ["creatorId"], { unique: false })
@Index("rank_level", ["rankLevel"], { unique: false })
@Index("opened_at", ["openedAt"], { unique: false })
@Index("created_at", ["createdAt"], { unique: false })
@Index("updated_at", ["updatedAt"], { unique: false })
export class LootBox {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({name: 'match_id', type: "bigint", nullable: false})
    matchId: number;

    @Column({ name: 'owner_id', type: 'int', nullable: false })
    ownerId: number;

    // 0: system auto create
    @Column({ name: 'creator_id', type: 'int', nullable: false })
    creatorId: number;

    @Column({ name: 'name', type: 'varchar', length: 80, nullable: true })
    name: string;

    @Column({ name: 'description', type: 'varchar', length: 255, nullable: true })
    description: string;

    @Column({ name: 'image_url', type: 'varchar', length: 255, nullable: true })
    imageUrl: string;

    // price = 0 -> cannot buy this loot box, just can get it from winning a match
    @Column({ name: 'price', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
    price: number;

    @Column({ name: 'rank_level', type: 'int', nullable: false, default: 0 })
    rankLevel: number;

    @Column({ name: 'reward_type', type: 'enum', enum: LootBoxRewardType, nullable: false})
    rewardType: string;

    @Column({ name: 'is_opened', type: 'boolean', nullable: false, default: false })
    isOpened: boolean;

    @Column({ name: 'status', type: 'enum', enum: LootBoxStatus, nullable: false, default: LootBoxStatus.LISTED})
    status: string;

    @Column({ name: 'is_deleted', type: 'boolean', nullable: false, default: false })
    isDeleted: boolean;

    @Column({ name: 'opened_at', type: 'bigint', nullable: true })
    openedAt: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        if (this.isOpened) {
            this.openedAt = nowInMillis();
        }
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}