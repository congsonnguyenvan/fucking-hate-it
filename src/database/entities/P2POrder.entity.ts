import { P2POrderFiat } from '../../shared/enums';
import { nowInMillis } from '../../shared/Utils';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BeforeInsert,
    BeforeUpdate,
} from 'typeorm';

@Entity('p2p_order')
export class P2POrder {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'user_id', type: 'int', nullable: false })
    userId: number;

    @Column({ name: 'payment_method_id', type: 'int', nullable: true })
    paymentMethodId: number;

    @Column({ name: 'amount',  type: 'decimal', precision:16, scale: 6, nullable: false })
    amount: number;

    @Column({ name: 'price',  type: 'decimal', precision:16, scale: 6, nullable: false })
    price: number;

    @Column({ name: 'fiat', type: 'varchar', nullable: true })
    fiat: P2POrderFiat;

    //to check if the minAmount have already been lower than maxAmount
    @Column({ name: 'amount_status', type:'tinyint', nullable: false, default: 1 })
    amountStatus: boolean;
     
    @Column({ name: 'total_lock_amount', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
    totalLockAmount: number;

    @Column({ name: 'min_amount', type: 'decimal', precision:16, scale: 6, nullable: false })
    minAmount: number;

    @Column({ name: 'max_amount', type: 'decimal', precision:16, scale: 6, nullable: false })
    maxAmount: number;

    @Column({ name: 'available', type: 'decimal', precision:16, scale: 6, nullable: false })
    available: number;

    @Column({ name: 'type', type: 'varchar', nullable: true })
    type: string;

    @Column({ name: 'status', type: 'varchar', nullable: false })
    status: string;

    //To check if order can be updated or not
    @Column({ name: 'new_order', type: 'tinyint', nullable: false, default: true })
    newOrder: boolean;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
