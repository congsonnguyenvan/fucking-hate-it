import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, Index, Unique } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('user')
@Index('email', ['email'], { unique: false })
@Index('status', ['status'], { unique: false })
@Index('phone', ['phone'], { unique: true})
export class User {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  id: number;

  @Column({ name: 'username', type: 'varchar', length: 80, nullable: false, unique: true })
  username: string;

  @Column({ name: 'nick_name', type: 'varchar', length: 80, nullable: true})
  nickName: string;

  @Column({ name: 'bio', type: 'varchar', length: 255, nullable: true })
  bio: string;

  // Acording to RFC 5321, maximum length is 256
  @Column({ name: 'email', type: 'varchar', length: 256, nullable: false, unique: true })
  email: string;

  @Column({ name: 'phone', type: 'varchar', length: 32, nullable: true})
  phone: string;

  @Column({ name: 'prefix_phone', type: 'varchar', length: 32, nullable: true})
  prefixPhone: string;

  @Column({ name: 'password', type: 'varchar', length: 256, nullable: false })
  password: string;

  @Column({ name: 'transaction_password', type: 'varchar', length: 256, nullable: false })
  transactionPassword: string;

  @Column({ name: 'avatar_url', type: 'varchar', length: 256, nullable: true })
  avatarUrl: string;

  @Column({ name: 'background_url', type: 'varchar', length: 255, nullable: true })
  backgroundUrl: string;

  @Column({ name: 'first_name', type: 'varchar', length: 100, nullable: true })
  firstName: string;

  @Column({ name: 'last_name', type: 'varchar', length: 100, nullable: true })
  lastName: string;

  @Column({ name: 'created_at', type: 'bigint', nullable: true })
  createdAt: number;

  @Column({ name: 'updated_at', type: 'bigint', nullable: true })
  updatedAt: number;

  @Column({ name: 'nonce', type: 'int', nullable: false, default: 0 })
  nonce: number;

  @Column({ name: 'is_active_2fa', type: 'tinyint', width: 1, nullable: false, default: 0 })
  public isActive2fa: boolean;

  @Column({ name: 'is_verify_email', type: 'tinyint', width: 1, nullable: false, default: 0 })
  public isVerifyEmail: boolean;

  @Column({ name: 'is_verify_phone_number', type: 'tinyint', width: 1, nullable: false, default: 0 })
  public isVerifyPhonenumber: boolean;

  @Column({ name: '2fa_secret', type: 'varchar', length: 256, nullable: true })
  twoFactorAuthenticationSecret: string;

  @Column({ name: 'login_code', type: 'varchar', length: 256, nullable: true })
  public loginCode: string;

  @Column({ name: 'reset_transac_code', type: 'varchar', length: 256, nullable: true })
  public resetTransactionCode: string;
  
  @Column({ name: 'status', type: 'varchar', length: 25, nullable: false, default: 'request' })
  status: string;

  @Column({ name: 'data', type: 'varchar', length: 100, nullable: true })
  public data: string;

  @Column({ name: 'badge', type: 'bigint', nullable: true })
  badge: number;



  
}
