import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('referral')
@Index('user_id', ['userId'], { unique: true })
@Index('referral_code', ['referralCode'], { unique: true })
export class Referral {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  id: number;

  @Column({ name: 'user_id', type: 'int', nullable: false, unique: true })
  userId: number;

  @Column({ name: 'referral_code', type: 'varchar', length: 256, nullable: false, unique: true })
  referralCode: string;

  @Column({ name: 'referral_level', type: 'int', nullable: false, default: 0 })
  referralLevel: number;

  @Column({ name: 'total_referral_user', type: 'int', nullable: false, default: 0 })
  totalReferralUser: number;

  @Column({ name: 'total_pending', type: 'int', nullable: false, default: 0 })
  totalPending: number;

  @Column({ name: 'total_referral_group', type: 'int', nullable: false, default: 0 })
  totalReferralGroup: number;

  @Column({ name: 'total_personal_nft', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
  totalPersonalNFT: number;

  @Column({ name: 'total_group_nft', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
  totalGroupNFT: number;

  @Column({ name: 'created_at', type: 'bigint', nullable: true })
  createdAt: number;

  @Column({ name: 'updated_at', type: 'bigint', nullable: true })
  updatedAt: number;

  @Column({ name: 'inviter', type: 'int', nullable: false })
  inviter: number;




  
}
