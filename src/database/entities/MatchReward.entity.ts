import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";

@Entity('match_reward')
@Index("match_id", ["matchId"], { unique: false })
@Index("user_id", ["userId"], { unique: false })
@Index("player_id", ["playerId"], { unique: false })
@Index("rewarded_at", ["rewardedAt"], { unique: false })
@Index("created_at", ["createdAt"], { unique: false })
@Index("updated_at", ["updatedAt"], { unique: false })
export class MatchReward {
    @PrimaryGeneratedColumn({ name: 'id', type: 'bigint' })
    id: number;

    @Column({name: 'match_id', type: 'bigint', nullable: false})
    matchId: number;

    @Column({name: 'user_id', type: 'bigint', nullable: false})
    userId: number;

    @Column({name: 'player_id', type: 'bigint', nullable: false})
    playerId: number;

    /**
     * [{
     *     "type": "WDT",
     *     "amount": 1
     *     "fee": 0.3 // optional
     * },
     * {
     *     "type": "loot_box",
     *     "id": "123"
     * },
     * {
     *     "type": "loot_box_key",
     *     "id": "123"
     * },
     * ]
     *
     * */
    @Column({name: 'reward', type: 'json', nullable: false})
    reward: any;

    // the time when the reward was given
    @Column({ name: 'rewarded_at', type: 'bigint', nullable: true })
    rewardedAt: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}
