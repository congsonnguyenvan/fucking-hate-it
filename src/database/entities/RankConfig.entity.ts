import { nowInMillis } from '../../shared/Utils';
import {
    Entity,
    Column,
    BeforeInsert,
    BeforeUpdate, PrimaryColumn,
} from 'typeorm';

@Entity('rank_config')
export class RankConfig {
    @PrimaryColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'level', type: 'int', nullable: false, unique: true })
    level: number;

    @Column({ name: 'title', type: 'varchar', length: 64, nullable: false })
    title: string;

    @Column({ name: 'description', type: 'text', nullable: true })
    description: string;

    @Column({ name: 'rank_threshold', type: 'bigint', nullable: false, unique: true })
    rankThreshold: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
