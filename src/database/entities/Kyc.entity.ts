import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";

@Entity("kyc")
@Index("user_id", ["userId"], {unique: true})
@Index("nationality", ["nationality"], {unique: false})
@Index("created_at", ["createdAt"], {unique: false})
@Index("updated_at", ["updatedAt"], {unique: false})
export class Kyc {
    @PrimaryGeneratedColumn({ name: "id", type: "bigint" })
    public id: number;

    @Column({ name: "user_id", type: "bigint", nullable: false, unique: false })
    public userId: number;

    @Column({ name: "first_name", type: "varchar", length: 100, nullable: true })
    public firstName: string;

    @Column({ name: "last_name", type: "varchar", length: 100, nullable: true })
    public lastName: string;

    // alpha-2 type
    @Column({ name: "nationality", type: "varchar", length: 4, nullable: false })
    public nationality: string;

    @Column({ name: "passport_id", type: "varchar", length: 100, nullable: true })
    public passportId: string;

    @Column({ name: "passport_image_url", type: "varchar", length: 255, nullable: true })
    public passportImageUrl: string;

    @Column({ name: "identity_card_id", type: "varchar", length: 100, nullable: true })
    public identityCardId: string;

    @Column({ name: "identity_card_image_url", type: "varchar", length: 255, nullable: true })
    public identityCardImageUrl: string;

    @Column({ name: "face_image_url", type: "varchar", length: 255, nullable: false })
    public faceImageUrl: string;

    @Column({ name: "bank_card_image_url", type: "varchar", length: 255, nullable: true })
    public bankCardImageUrl: string;

    //pending, approved, rejected
    @Column({ name: "status", type: "varchar", length: 20, nullable: false, default: "pending" })
    public status: string;

    @Column({ name: "confirmed_at", type: "bigint", nullable: true })
    public  confirmedAt: number;

    @Column({ name: "created_at", type: "bigint", nullable: false })
    public createdAt: number;

    @Column({ name: "updated_at", type: "bigint", nullable: false })
    public updatedAt: number;



    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}