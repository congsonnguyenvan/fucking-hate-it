import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('referral_reward')
@Index('referral_level', ['referralLevel'], { unique: true })
@Index('referral_title', ['referralTitle'], { unique: true })
export class ReferralReward {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  id: number;

  @Column({ name: 'referral_level', type: 'int', nullable: false, unique: true })
  referralLevel: number;

  @Column({ name: 'referral_title', type: 'varchar', length: 256, nullable: false, unique: true })
  referralTitle: string;

  @Column({ name: 'min_referral_user', type: 'int', nullable: false, default: 0 })
  minReferralUser: number;

  @Column({ name: 'min_referral_group', type: 'int', nullable: false, default: 0 })
  minReferralGroup: number;

  @Column({ name: 'min_personal_nft', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
  minPersonalNFT: number;

  @Column({ name: 'min_group_nft', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
  minGroupNFT: number;

  @Column({ name: 'dividends_required', type: 'int', nullable: true, default: 0 })
  dividendsRequired: number;

  @Column({ name: 'created_at', type: 'bigint', nullable: true })
  createdAt: number;

  @Column({ name: 'updated_at', type: 'bigint', nullable: true })
  updatedAt: number;

  @Column({ name: 'daily_reward', type: 'decimal', precision:10, scale: 2, nullable: false })
  dailyReward: number;




  
}
