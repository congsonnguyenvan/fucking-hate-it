import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";

@Entity('player_log')
@Index('match_id', ['matchId'], { unique: false })
@Index('user_id', ['userId'], { unique: false })
@Index('player_id', ['playerId'], { unique: false })
@Index('created_at', ['createdAt'], { unique: false })
@Index('updated_at', ['updatedAt'], { unique: false })
export class PlayerLog {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({name: 'match_id', type: "bigint", nullable: false})
    matchId;

    @Column({name: 'user_id', type: "bigint", nullable: false})
    userId;

    @Column({name: 'player_id', type: "bigint", nullable: false})
    playerId;

    @Column({ name: 'is_ready', type: 'boolean', nullable: true })
    isReady: boolean;

    @Column({ name: 'is_winner', type: 'boolean', nullable: true })
    isWinner: boolean;

    // owner
    // other
    @Column({name: 'type', type: "varchar", nullable: false})
    type: string;

    @Column({ name: 'total_guesses', type: 'int', default: 0, nullable: false })
    totalGuesses: number;

    @Column({ name: 'word_pair', type: 'tinyint', nullable: true })
    wordPair: number;

    @Column({ name: 'energy', type: 'int', default: 0, nullable: false })
    energy: number;

    @Column({ name: 'previous_guessed_word', type: 'json', nullable: true})
    previousGuessedWord: any;

    @Column({ name: 'previous_history', type: 'json', nullable: true})
    previousHistory: any;

    @Column({ name: 'history', type: 'json', nullable: true })
    history: any;

    @Column({ name: 'guessed_word', type: 'json', nullable: true })
    guessedWord: any;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}