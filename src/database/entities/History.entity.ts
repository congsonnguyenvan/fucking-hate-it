import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";
import { HistoryTag } from '../../shared/enums';

@Entity('history')
export class History {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'user_id', type: 'int', nullable: true })
    userId: number;

    @Column({ name: 'target_id', type: 'int', nullable: true })
    targetId: number;

    @Column({ name: 'tag', type: 'enum', enum: HistoryTag, nullable: true })
    tag: HistoryTag;

    @Column({ name: 'type', type: 'text', nullable: true })
    type: string;

    @Column({ name: 'activity', type: 'text', nullable: true })
    activity: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}