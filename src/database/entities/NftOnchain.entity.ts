import { NftStatus, Ranks } from '../../shared/enums';
import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Index, Unique } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('nft_onchain')
@Index('chain_id', ['chainId'], { unique: false })
@Index('t', ['chainId', 'nftAddress'], { unique: true })
@Index('image', ['image'], { unique: false })
@Index('metadata_updated_at', ['metadataUpdatedAt'], { unique: false })
@Index('status', ['status'], { unique: false })
@Index('display_status', ['displayStatus'], { unique: false })
export class NftOnchain {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'chain_id', type: 'varchar', nullable: true })
    public chainId: string;

    @Column({ name: 'level', type: 'int', nullable: false, default: 1 })
    public level: number;

    @Column({ name: 'rank', type: 'varchar', length: 50, nullable: false, default: Ranks.IRON })
    rank: string;

    @Column({ name: 'nft_address', type: 'varchar', nullable: true })
    public nftAddress: string;

    @Column({ name: 'creator', type: 'varchar', length: 255, nullable: true })
    public creator: string;

    @Column({ name: 'metadata_address', type: 'varchar', length: 255, nullable: true })
    public metadataAddress: string;

    @Column({ name: 'char', type: 'varchar', length: 50, nullable: true })
    public char: string;

    @Column({ name: 'name', type: 'varchar', length: 255, nullable: true })
    public name: string;

    @Column({ name: 'image', type: 'varchar', length: 255, nullable: true })
    public image: string;
  
    @Column({ name: 'raw_data', type: 'varchar', length: 5000, nullable: true })
    public rawData: string;

    @Column({ name: 'status', type: 'enum', enum: NftStatus, nullable: true, default: NftStatus.LISTED })
    public status: string;

    @Column({ name: 'display_status', type: 'varchar', length: 50, nullable: true })
    public displayStatus: string;
    
    @Column({ name: 'metadata_updated_at', type: 'bigint', nullable: true })
    public metadataUpdatedAt: number;
    
    @Column({ name: 'ipfs_updated_at', type: 'bigint', nullable: true })
    public ipfsUpdatedAt: number;

    @Column({ name: 'is_ipfs_updated', type: 'bool', nullable: false, default: false })
    public isIpfsUpdated: boolean;

    @Column({ name: 'is_metadata_updated', type: 'bool', nullable: false, default: false })
    public isMetadataUpdated: boolean;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    public createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    public updatedAt: number;

    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
