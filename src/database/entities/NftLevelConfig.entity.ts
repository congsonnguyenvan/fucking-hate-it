import { NftConfigStatus, Ranks } from '../../shared/enums';
import { nowInMillis } from '../../shared/Utils';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BeforeInsert,
    BeforeUpdate,
} from 'typeorm';

@Entity('nft_level_config')
export class NftLevelConfig {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'rank', type: 'varchar', length: 50, nullable: false, unique: true, default: Ranks.IRON })
    rank: string;

    @Column({ name: 'level', type: 'int', nullable: false, default: 1 })
    level: number;

    @Column({ name: 'nft_price', type: 'decimal', precision:16, scale: 6, nullable: false })
    nftPrice: number;

    @Column({ name: 'earning_rate', type: 'decimal', precision:16, scale: 6, nullable: false })
    earningRate: number;

    @Column({ name: 'weight', type: 'decimal', precision:16, scale: 6, nullable: false })
    weight: number;

    @Column({ name: 'purchase_limit', type: 'int', nullable: false })
    purchaseLimit: number;

    @Column({ name: 'cycle', type: 'int', nullable: false })
    cycle: number;

    @Column({ name: 'status', type: 'enum', enum: NftConfigStatus, nullable: false, default: NftConfigStatus.ACTIVE})
    status: NftConfigStatus;

    @Column({ name: 'min_grade', type: 'int', nullable: false, default: 1 })
    minGrade: number;

    @Column({ name: 'created_by', type: 'int', nullable: false })
    createdBy: number;

    @Column({ name: 'updated_by', type: 'int', nullable: false })
    updatedBy: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
