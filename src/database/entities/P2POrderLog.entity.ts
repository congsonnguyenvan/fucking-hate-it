import { nowInMillis } from '../../shared/Utils';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BeforeInsert,
    BeforeUpdate,
} from 'typeorm';

@Entity('p2p_order_log')
export class P2POrderLog {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'user_id', type: 'int', nullable: false })
    userId: number;

    @Column({ name: 'p2p_order_id', type: 'int', nullable: false })
    p2pOrderId: number;

    @Column({ name: 'payment_method_id', type: 'int', nullable: true })
    paymentMethodId: number;

    @Column({ name: 'amount',  type: 'decimal', precision:16, scale: 6, nullable: false })
    amount: number;

    @Column({ name: 'price',  type: 'decimal', precision:16, scale: 6, nullable: false })
    price: number;

    @Column({ name: 'fiat', type: 'varchar', nullable: true })
    fiat: string;

    @Column({ name: 'amount_status', type:'tinyint', nullable: false, default: 1 })
    amountStatus: boolean;
     
    @Column({ name: 'total_lock_amount', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
    totalLockAmount: number;

    @Column({ name: 'min_amount', type: 'decimal', precision:16, scale: 6, nullable: false })
    minAmount: number;

    @Column({ name: 'max_amount', type: 'decimal', precision:16, scale: 6, nullable: false })
    maxAmount: number;

    @Column({ name: 'available', type: 'decimal', precision:16, scale: 6, nullable: false })
    available: number;

    @Column({ name: 'type', type: 'varchar', nullable: true })
    type: string;

    @Column({ name: 'status', type: 'varchar', nullable: false })
    status: string;

    @Column({ name: 'new_order', type: 'tinyint', nullable: false, default: true })
    newOrder: boolean;
    //if id = 0 then it would be system wallet
    @Column({ name: 'sender_wallet', type: 'varchar', nullable: true, default: null })
    senderWallet: string;

    @Column({ name: 'receiver_wallet', type: 'varchar', nullable: true, default: null })
    receiverWallet: string;

    @Column({ name: 'send_amount', type: 'decimal', precision:16, scale: 6, nullable: true, default: null })
    sendAmount: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
