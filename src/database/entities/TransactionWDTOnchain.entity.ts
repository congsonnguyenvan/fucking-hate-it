import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('transaction_wdt_onchain')
export class TransactionWDTOnchain {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int', unsigned: true })
  public id: number;

  @Column({ name: 'block_time', type: 'bigint', nullable: true })
  public blockTime: number;

  @Column({ name: 'fee', type: 'int', nullable: true })
  public fee: number;

  @Column({ name: 'instruction', type: 'varchar', nullable: true })
  public instruction: string;

  @Column('varchar', { name: 'from_address', nullable: true })
  public fromAddress: string;

  @Column('varchar', { name: 'to_address', nullable: true })
  public toAddress: string;

  @Column('bigint', { name: 'from_address_pre_token_balance', nullable: true })
  public fromAddressPreTokenBalance: number;

  @Column('bigint', { name: 'to_address_pre_token_balance', nullable: true })
  public toAddressPreTokenBalance: number;

  @Column('bigint', { name: 'from_address_post_token_balance', nullable: true })
  public fromAddressPostTokenBalance: number;

  @Column('bigint', { name: 'to_address_post_token_balance', nullable: true })
  public toAddressPostTokenBalance: number;

  @Column({ name: 'decimals', type: 'int', nullable: true })
  public decimals: number;

  @Column({ name: 'slot', type: 'bigint', nullable: true })
  public slot: number;

  @Column({ name: 'signer_address', type: 'varchar', nullable: true })
  public signerAddress: string;

  @Column({ name: 'signature', type: 'varchar', length: 250, nullable: true })
  public signature: string;

  //error if transaction failed, null if succeeded
  @Column('varchar', { length: 5000, name: 'err', nullable: true })
  public err: string | null;

  @Column({ name: 'created_at', type: 'bigint', nullable: true })
  public createdAt: number;

  @Column({ name: 'updated_at', type: 'bigint', nullable: true })
  public updatedAt: number;



  
}
