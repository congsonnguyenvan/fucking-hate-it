import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('direct_referral_bonus')
export class DirectReferralBonus {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
  id: number;

  @Column({ name: 'user_id', type: 'int', nullable: false })
  userId: number;

  @Column({ name: 'stake_log_id' , type: 'int', nullable: true })
  stakeLogId: number;

  @Column({ name: 'amount_available', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
  amountAvailable: number;

  @Column({ name: 'amount_received', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
  amountReceived: number;

  @Column({ name: 'created_at', type: 'bigint', nullable: true })
  createdAt: number;

  @Column({ name: 'updated_at', type: 'bigint', nullable: true })
  updatedAt: number;

  @Column({ name: 'inviter', type: 'int', nullable: false })
  inviter: number;




  
}
