import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";
import {MatchRewardType} from "../../shared/enums";

@Entity('player')
@Index('match_id', ['matchId'], { unique: false })
@Index('user_id', ['userId'], { unique: false })
@Index('created_at', ['createdAt'], { unique: false })
@Index('updated_at', ['updatedAt'], { unique: false })
export class Player {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({name: 'match_id', type: "bigint", nullable: false})
    matchId: number;

    @Column({name: 'user_id', type: "bigint", nullable: false})
    userId: number;

    // owner
    // other
    @Column({name: 'type', type: "varchar", nullable: false})
    type: string;

    @Column({name: 'is_winner', type: "boolean", default: false, nullable: false})
    isWinner: boolean;

    @Column({ name: 'reward_type', type: 'enum', enum: MatchRewardType, nullable: true })
    rewardType: string;

    @Column( {name: 'winner_received', type: 'decimal', precision:18, scale: 6, default: 0, nullable: true})
    winnerReceived: number;

    // 1 or 2
    // 0 means 1v1 or classic
    @Column({ name: 'team', type: 'smallint', default: 0, nullable: true })
    team: number;

    // 1 or 2 or 3 or 4 or 5
    // 0 means 1v1 or classic
    @Column( { name: 'word_pair', type: 'smallint',default: 0, nullable: true })
    wordPair: number;

    @Column( { name: 'kill_count', type: 'smallint', default: 0, nullable: true })
    killCount: number;

    @Column( { name: 'death_count', type: 'smallint', default: 0, nullable: true })
    deathCount: number;

    @Column({ name: 'bet_amount', type: 'int', nullable: true, default: 0})
    betAmount: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}