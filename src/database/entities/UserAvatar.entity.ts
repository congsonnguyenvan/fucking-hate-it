import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('user_avatar')
export class UserAvatar {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'user_id', type: 'int', nullable: false })
    userId: number;

    @Column({ name: 'avatar_id', type: 'int', nullable: false })
    avatarId: number;

    @Column({ name: 'avatar_name', type: 'varchar', length: 255, nullable: true })
    avatarName: string;

    @Column({ name: 'avatar_url', type:'varchar', nullable: false})
    avatarURL: string;

    @Column({ name: 'status', type: 'varchar', nullable: true })
    status: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}
