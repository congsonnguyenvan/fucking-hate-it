import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";
import {IDictionaryInterface} from "../interfaces/IDictionary.interface";

@Entity("dictionary")
@Index("word", ["word"], { unique: true })
@Index("created_at", ["createdAt"], { unique: false })
@Index("updated_at", ["updatedAt"], { unique: false })
export class Dictionary implements IDictionaryInterface {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'word', type: 'varchar', length: 7, nullable: false })
    word: string;

    @Column({ name: 'meanings', type: 'json',  nullable: true })
    meanings: any;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}