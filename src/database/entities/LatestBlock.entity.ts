import { nowInMillis } from '../../shared/Utils';
import { Entity, PrimaryColumn, Column, BeforeUpdate, BeforeInsert } from 'typeorm';

@Entity('latest_block')
export class LatestBlock {
  @PrimaryColumn()
  public currency: string;

  @Column({ name: 'signature', nullable: false })
  public signature: string;

  @Column({ name: 'created_at', type: 'bigint' })
  public createdAt: number;

  @Column({ name: 'updated_at', type: 'bigint' })
  public updatedAt: number;



  
}
