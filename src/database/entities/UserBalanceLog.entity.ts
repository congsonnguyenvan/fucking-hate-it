import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';
import { UserBalanceLogType } from '../../shared/enums';

@Entity('user_balance_log')
export class UserBalanceLog {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'user_id', type: 'int', nullable: false })
    userId: number;

    @Column({ name: 'trading_account_change', type: 'tinyint', nullable: false, default: false })
    tradingAccountChange: boolean;
  
    @Column({ name: 'gaming_account_change', type: 'tinyint', nullable: false, default: false })
    gamingAccountChange: boolean;

    @Column({ name: 'type', type: 'enum', enum: UserBalanceLogType, nullable: false, default: UserBalanceLogType.WDT_TRANSFER })
    type: UserBalanceLogType;

    @Column({ name: 'target_id', type: 'int', nullable: false })
    targetId: number;
  
    @Column({ name: 'amount', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
    amount: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}
