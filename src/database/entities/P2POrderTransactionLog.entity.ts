import { nowInMillis } from '../../shared/Utils';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BeforeInsert,
    BeforeUpdate,
} from 'typeorm';

@Entity('p2p_order_transaction_log')
export class P2POrderTransactionLog {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'user_id', type: 'int', nullable: false })
    userId: number;

    @Column({ name: 'p2p_order_transaction_id', type: 'int', nullable: false })
    p2pOrderTransactionId: number;

    @Column({ name: 'p2p_order_id', type: 'int', nullable: false })
    p2pOrderId: number;

    @Column({ name: 'amount', type: 'bigint', nullable: false })
    amount: number;

    @Column({ name: 'status', type: 'varchar', nullable: false })
    status: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
