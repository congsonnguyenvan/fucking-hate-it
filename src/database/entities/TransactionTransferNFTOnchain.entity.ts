import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('transaction_transfer_nft_onchain')
export class TransactionTransferNFTOnchain {
  @PrimaryGeneratedColumn({ name: 'id', type: 'int', unsigned: true })
  public id: number;
 
  @Column({ name: 'fee', type: 'int', nullable: true })
  public fee: number;

  @Column({ name: 'instruction', type: 'varchar', nullable: true })
  public instruction: string;

  @Column('varchar', { name: 'from_address', nullable: true })
  public fromAddress: string;

  @Column('varchar', { name: 'to_address', nullable: true })
  public toAddress: string;

  @Column('varchar', { name: 'nft_address', nullable: true })
  public nftAddress: string;

  @Column({ name: 'amount', type: 'int', nullable: true })
  public amount: number;

  @Column({ name: 'slot', type: 'bigint', nullable: true })
  public slot: number;

  @Column({ name: 'signer_address', type: 'varchar', nullable: true })
  public signerAddress: string;

  @Column({ name: 'signature', type: 'varchar', length: 250, nullable: true })
  public signature: string;

  //error if transaction failed, null if succeeded
  @Column('varchar', { length: 5000, name: 'err', nullable: true })
  public err: string | null;

  @Column({ name: 'created_at', type: 'bigint', nullable: true })
  public createdAt: number;

  @Column({ name: 'updated_at', type: 'bigint', nullable: true })
  public updatedAt: number;



  
}
