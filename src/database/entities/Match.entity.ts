import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";

@Entity('match')
@Index("winner", ["winner"], { unique: false })
@Index("ended_at", ["endedAt"], { unique: false })
@Index("started_at", ["startedAt"], { unique: false })
@Index("created_at", ["createdAt"], { unique: false })
@Index("updated_at", ["updatedAt"], { unique: false })
export class Match {
    @PrimaryGeneratedColumn({ name: 'id', type: 'bigint' })
    id: number;

    @Column({name: 'words', type: 'json', nullable: false})
    words: any;

    // classic
    // battle
    // 1v1
    // 2v2
    // 3v3
    // 4v4
    // 5v5
    @Column({name: 'type', type: 'varchar', length: 20, nullable: false})
    type: string;

    @Column({ name: 'rank_level', type: 'int', nullable: true })
    rankLevel: number;

    // pending
    // playing
    // finished
    @Column({name: 'status', type: 'varchar', length: 20, nullable: false})
    status: string;

    @Column({name: 'join_code', type: 'varchar', length: 20, nullable: false})
    joinCode: string;

    // total bet amount of all player in match
    @Column( {name: 'prize_pool', type: 'int', nullable: true, default: 0})
    prizePool: number;

    // 0: computer
    @Column({name: 'winner', type: "bigint", nullable: true})
    winner: number

    @Column({ name: 'started_at', type: 'bigint', nullable: true })
    startedAt: number;

    @Column({ name: 'ended_at', type: 'bigint', nullable: true })
    endedAt: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}