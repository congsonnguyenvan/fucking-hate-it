import {BeforeInsert, BeforeUpdate, Column, Entity, Index, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";
import {LootBoxStatus} from "../../shared/enums";

@Entity("loot_box_key_onchain")
@Index("key_offchain_id", ["keyOffchainId"], { unique: false })
@Index("nft_address", ["nftAddress"], { unique: false })
@Index("metadata_address", ["metadataAddress"], { unique: false })
@Index("created_at", ["createdAt"], { unique: false })
@Index("updated_at", ["updatedAt"], { unique: false })
export class LootBoxKeyOnchain {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'key_offchain_id', type: 'int', nullable: false })
    keyOffchainId: number;

    @Column({ name: 'name', type: 'varchar', length: 255, nullable: true })
    public name: string;

    @Column({ name: 'nft_address', type: 'varchar', nullable: true })
    public nftAddress: string;

    @Column({ name: 'metadata_address', type: 'varchar', length: 255, nullable: true })
    public metadataAddress: string;

    @Column({ name: 'image', type: 'varchar', length: 255, nullable: true })
    public image: string;

    @Column({ name: 'raw_data', type: 'varchar', length: 5000, nullable: true })
    public rawData: string;

    @Column({ name: 'creator', type: 'varchar', length: 255, nullable: true })
    public creator: string;

    @Column({ name: 'status', type: 'enum', enum: LootBoxStatus, nullable: true, default: LootBoxStatus.LISTED })
    public status: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}