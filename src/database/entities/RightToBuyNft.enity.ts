import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";
import {RightToBuyNftStatus} from "../../shared/enums";

@Entity('right_to_buy_nft')
export class RightToBuyNft {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    // 0: system auto create
    @Column({ name: 'creator_id', type: 'int', nullable: false, default: 0 })
    creatorId: number;

    // 0: do not have owner
    @Column({ name: 'owner_id', type: 'int', nullable: false, default: 0 })
    ownerId: number;

    @Column({ name: 'name', type: 'varchar', length: 80, nullable: true })
    name: string;

    @Column({ name: 'image_url', type: 'varchar', length: 255, nullable: true })
    imageUrl: string;

    @Column({ name: 'rank_level', type: 'int', nullable: false, default: 0 })
    rankLevel: number;

    @Column({ name: 'price', type: 'decimal', precision: 16, scale: 6, nullable: true, default: 0})
    price: number;

    @Column({ name: 'status', type: 'enum', enum: RightToBuyNftStatus, nullable: true, default: RightToBuyNftStatus.ACTIVATED})
    status: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}