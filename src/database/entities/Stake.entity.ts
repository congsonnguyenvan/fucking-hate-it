import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";

@Entity('stake')
export class Stake {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'user_id', type: 'int', nullable: true })
    userId: number;

    @Column({ name: 'total_pool', type: 'decimal', precision:18, scale: 6, default: 0, nullable: true })
    totalPool: number;

    @Column({ name: 'nft_price', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
    nftPrice: number;

    @Column({ name: 'earning_rate', type: 'decimal', precision:16, scale: 6, nullable: true })
    earningRate: number;

    @Column({ name: 'cycle', type: 'int', nullable: true })
    cycle: number;

    @Column({ name: 'weight', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0.1 })
    weight: number;

    @Column({ name: 'nft_address', type: 'varchar', nullable: true })
    nftAddress: string;

    @Column({ name: 'total_claimed', type: 'decimal', precision:18, scale: 6, default: 0, nullable: true })
    totalClaimed: number;

    @Column({ name: 'status', type: 'tinyint', nullable: true, default: 1})
    status: number;

    @Column({ name: 'staked_at', type: 'bigint', nullable: true })
    stakedAt: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}