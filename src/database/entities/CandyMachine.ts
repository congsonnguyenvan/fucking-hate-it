import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';


export enum CandyMachineStatus {
    PENDING = 'pending',
    PROCESSING = 'processing',
    COMPLETED = 'completed',
    FAILED = 'failed',
}
@Index('candy', ['collectionAddress', 'candyMachineAddress'], { unique: true })
@Entity('candy_machine')
export class CandyMachineItem {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    // letter
    // badge
    // loot_box
    // loot_box_key
    @Column({ name: 'type', type:'varchar', length: 50, nullable: true })
    type: string;

    // has value if type is loot_box_key or loot_box to map with offchain object
    @Column({ name: 'offchain_id', type: 'int', nullable: true })
    offchainId: number;

    @Column({ name: 'collection_address', type: 'varchar', length: 255, nullable: false })
    collectionAddress: string;

    @Column({ name: 'minted', type:'bigint', nullable: false, default: 0 })
    minted: number;

    @Column({ name: 'to_mint', type:'bigint', nullable: false, default: 1 })
    toMint: number;

    @Column({ name: 'metadata_url', type: 'varchar', length: 500, nullable: true })
    metadataUrl: string;
    
    @Column({ name: 'status', type: 'enum', enum: CandyMachineStatus, nullable: false, default: CandyMachineStatus.PENDING})
    status: CandyMachineStatus;

    @Column({ name: 'metadata_name', type: 'varchar', length: 255, nullable: true })
    metadataName: string;

    @Column({ name: 'candy_machine_address', type: 'varchar', length: 255, nullable: true })
    candyMachineAddress: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}
