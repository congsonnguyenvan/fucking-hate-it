import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Index, Unique } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('nft_owner')
@Index('chain_id', ['chainId'], { unique: false })
@Index('nft_address', ['nftAddress'], { unique: false })
@Index('owner', ['owner'], { unique: false })
export class NftOwner {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'chain_id', type: 'varchar', nullable: true })
    public chainId: string;

    @Column({ name: 'nft_address', type: 'varchar', nullable: false })
    public nftAddress: string;

    @Column({ name: 'owner', type: 'varchar', length: 255, nullable: true })
    owner: string;

    @Column({ name: 'block_timestamp', type: 'bigint', nullable: true })
    public blockTimestamp: number;
    
    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    public createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    public updatedAt: number;

    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
