import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {IMatchBetConfig} from "../interfaces/IMatchBetConfig.interface";
import {MatchType} from "../../shared/enums";
import {nowInMillis} from "../../shared/Utils";

@Entity("match_bet_config")
export class MatchBetConfig implements IMatchBetConfig {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'match_type', type: 'enum', enum: MatchType, nullable: false })
    matchType: string;

    @Column({ name: 'rank_level', type: 'int', nullable: false })
    rankLevel: number;

    //WDT
    @Column({ name: 'amount_per_match', type: 'decimal', precision: 16, scale: 6, nullable: false })
    amountPerMatch: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}