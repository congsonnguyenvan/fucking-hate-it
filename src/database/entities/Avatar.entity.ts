import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';

@Entity('avatar')
export class Avatar {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'admin_id', type: 'int', nullable: false })
    adminId: number;

    @Column({ name: 'avatar_name', type: 'varchar', length: 255, nullable: true })
    avatarName: string;

    @Column({ name: 'avatar_url', type:'varchar', nullable: false})
    avatarURL: string;

    @Column({ name: 'status', type: 'tinyint', nullable: true, default: true })
    status: boolean;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}
