import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';
// import Kms from '../encrypt/Kms';

@Entity('collection')
export class Collection {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    // letter
    // badge
    // loot_box
    // loot_box_key
    @Column({ name: 'type', type:'varchar', length: 50, nullable: true })
    type: string;

    @Column({ name: 'collection_address', type: 'varchar', length: 255, nullable: false })
    collectionAddress: string;

    @Column({ name: 'minter', type: 'varchar', length: 255, nullable: false })
    minter: string;

    @Column({ name: 'avatar_url', type: 'varchar', length: 255, nullable: false })
    avatarUrl: string;

    @Column({ name: 'name', type: 'varchar', length: 100, nullable: false })
    name: string;

    @Column({ name: 'description', type: 'varchar', length: 255, nullable: false })
    description: string;

    @Column({ name: 'symbol', type: 'varchar', length: 10, nullable: false })
    symbol: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}
