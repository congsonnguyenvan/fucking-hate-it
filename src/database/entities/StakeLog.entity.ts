import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryColumn, PrimaryGeneratedColumn} from "typeorm";
import {nowInMillis} from "../../shared/Utils";
import { StakeLogType } from '../../shared/enums';

@Entity('stake_log')
export class StakeLog {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'user_id', type: 'int', nullable: true })
    userId: number;

    @Column({ name: 'amount', type: 'decimal', precision:18, scale: 6, default: 0, nullable: true })
    amount: number;

    @Column({ name: 'amount_received', type: 'decimal', precision:18, scale: 6, default: 0, nullable: true })
    amountReceived: number;

    @Column({ name: 'nft_address', type: 'varchar', nullable: true })
    nftAddress: string;

    @Column({ name: 'stake_id' , type: 'int', nullable: true })
    stakeId: number;

    @Column({ name: 'status', type: 'tinyint', nullable: true, default: 1})
    status: number;

    @Column({ name: 'calculated', type: 'tinyint', nullable: false, default: 0})
    calculated: number;

    @Column({ name: 'type', type: 'enum', enum: StakeLogType, nullable: false, default: StakeLogType.STAKE})
    type: StakeLogType;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;


    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}