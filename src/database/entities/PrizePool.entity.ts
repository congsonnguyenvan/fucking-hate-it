import { nowInMillis } from '../../shared/Utils';
import {
    Entity,
    Column,
    BeforeInsert,
    BeforeUpdate, PrimaryColumn, PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('prize_pool')
export class PrizePool {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'prize_pool_total', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0})
    prizePoolTotal: number;

    @Column({ name: 'referral_amount', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0})
    referralAmount: number;

    @Column({ name: 'project_amount', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0})
    projectAmount: number;

    @Column({ name: 'burn_amount', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0})
    burnAmount: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
