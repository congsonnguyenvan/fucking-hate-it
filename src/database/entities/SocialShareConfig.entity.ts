import {BeforeInsert, BeforeUpdate, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {MatchType} from "../../shared/enums";
import {nowInMillis} from "../../shared/Utils";
import {ISocialShareConfig} from "../interfaces/ISocialShareConfig.interface";

@Entity("social_share_config")
export class SocialShareConfig implements ISocialShareConfig {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    id: number;

    @Column({ name: 'match_type', type: 'enum', enum: MatchType, nullable: false })
    matchType: string;

    @Column({ name: 'rate', type: 'decimal', precision: 16, scale: 6, nullable: false, default: 2})
    rate: number;

    @Column({ name: 'start_time', type: 'bigint', nullable: false })
    startTime: number;

    @Column({ name: 'end_time', type: 'bigint', nullable: false })
    endTime: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: true })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: true })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}