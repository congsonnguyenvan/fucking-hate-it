import { NftOrderStatus } from '../../shared/enums';
import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';
import { truncate } from 'fs';

export const enum OnchainServiceStatus{
    PENDING = 'pending',
    PROCESSING = 'processing',
    SUCCEEDED = 'succeeded',
    FAILED = 'failed'
}

export const enum OnchainTransactionType{
    NFT = 'NFT',
    WDT = 'WDT',
}

@Entity('onchain_service')
export class OnchainService {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;
    
    @Column({ name: 'address', type: 'varchar', nullable: false })
    address: string;

    @Column({ name: 'secret_key', type: 'varchar', length: 500, nullable: true })
    secretKey: string;

    @Column({ name: 'onchain_transaction_type', type: 'varchar', nullable: false })
    onchainTransactionType: OnchainTransactionType;

    @Column({ name: 'order_id', type: 'int', nullable: false })
    orderId: number;

    @Column({ name: 'status', type: 'varchar', nullable: false, default: OnchainServiceStatus.PENDING})
    status: OnchainServiceStatus;

    @Column({ name: 'sender', type: 'varchar',length:255, nullable: false})
    sender: string;

    @Column({ name: 'receiver', type: 'varchar',length:255, nullable: false})
    receiver: string;

    @Column({ name: 'signature', type: 'varchar',length:255, nullable: true})
    signature: string;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    public createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    public updatedAt: number;

    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
