import { nowInMillis } from '../../shared/Utils';
import {
    Entity,
    Column,
    BeforeInsert,
    BeforeUpdate, PrimaryColumn, PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('prize_pool_distribution')
export class PrizePoolDistribution {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'prize_pool_portion', type: 'decimal', precision:9, scale: 6, nullable: false, default: 0})
    prizePoolPortion: number;

    @Column({ name: 'referral_portion', type: 'decimal', precision:9, scale: 6, nullable: false, default: 0})
    referralPortion: number;

    @Column({ name: 'project_portion', type: 'decimal', precision:9, scale: 6, nullable: false, default: 0})
    projectPortion: number;

    @Column({ name: 'burn_portion', type: 'decimal', precision:9, scale: 6, nullable: false, default: 0})
    burnPortion: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
