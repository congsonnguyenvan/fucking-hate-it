import { Entity, PrimaryGeneratedColumn, Column, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';
@Entity('badge')
@Index('user_id', ['userId'], { unique: true })
export class Badge {
    @PrimaryGeneratedColumn({ name: 'id', type: 'bigint' })
    id: number;

    @Column({ name: 'name', type: 'varchar', length: 80, nullable: false })
    name: string;

    @Column({ name: 'description', type: 'text', length: 255, nullable: false })
    description: string;

    @Column({ name: 'image_url', type: 'varchar', length: 255, nullable: false })
    imageUrl: string;

    @Column({ name: 'user_id', type: 'bigint', nullable: false })
    userId: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdateDates() {
        this.updatedAt = nowInMillis();
    }
}
