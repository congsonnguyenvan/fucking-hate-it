import { nowInMillis } from '../../shared/Utils';
import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    BeforeInsert,
    BeforeUpdate,
} from 'typeorm';
export const enum TransactionStatus {
    TRANSFERRING = 'transferring',
    TRANSFERRED = 'transferred',
    CONFIRMED = 'confirmed',
    CANCELED = 'canceled'
}

export const enum TransactionType {
    SELL = 'sell',
    BUY = 'buy',
}

const innerPayTime = 15 * 60000;
@Entity('p2p_order_transaction')
export class P2POrderTransaction {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'user_id', type: 'int', nullable: false })
    userId: number;

    @Column({ name: 'order_owner_id', type: 'int', nullable: false })
    orderOwnerId: number;

    @Column({ name: 'p2p_order_id', type: 'int', nullable: false })
    p2pOrderId: number;

    @Column({ name: 'amount', type: 'decimal', precision:16, scale: 6, nullable: false })
    amount: number;

    @Column({ name: 'lock_amount', type: 'decimal', precision:16, scale: 6, nullable: true })
    lockAmount: number;

    @Column({ name: 'payment_method_id', type: 'int', nullable: true })
    paymentMethodId: number;

    @Column({ name: 'status', type: 'varchar', nullable: true })
    status: TransactionStatus;

    @Column({ name: 'type', type: 'varchar', nullable: false })
    type: TransactionType;

    @Column({ name: 'pay_time', type: 'bigint', nullable: false })
    payTime: number;

    @Column({ name: 'deposit_confirm_time', type: 'bigint', nullable: true })
    depositConfirmTime: number;

    @Column({ name: 'receive_confirm_time', type: 'bigint', nullable: true })
    receiveConfirmTime: number;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    updatedAt: number;

    @BeforeInsert()
    public updateCreateDates() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
        this.payTime = nowInMillis() + innerPayTime;
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
