import { NftOrderStatus } from '../../shared/enums';
import { Entity, Column, PrimaryGeneratedColumn, BeforeInsert, BeforeUpdate, Index } from 'typeorm';
import { nowInMillis } from '../../shared/Utils';
import { truncate } from 'fs';

@Entity('nft_order')
@Index('status', ['status'], { unique: false })
export class NftOrder {
    @PrimaryGeneratedColumn({ name: 'id', type: 'int' })
    public id: number;

    @Column({ name: 'seller_id', type: 'int', nullable: false })
    sellerId: number;

    @Column({ name: 'wallet', type: 'varchar', nullable: true })
    public wallet: string;

    @Column({ name: 'buyer_id', type: 'int', nullable: true })
    buyerId: number;

    @Column({ name: 'nft_id', type: 'int', nullable: false })
    public nftId: number;

    @Column({ name: 'price', type: 'decimal', precision:16, scale: 6, nullable: false })
    public price: number; 

    @Column({ name: 'lock_amount', type: 'decimal', precision:16, scale: 6, nullable: false, default: 0 })
    public lockAmount: number; 

    @Column({ type: 'tinyint', width: 1, name: 'system_nft', nullable: false, default: false })
    public systemNft: boolean;

    @Column({ name: 'status', type: 'enum', enum: NftOrderStatus, nullable: false, default: NftOrderStatus.LISTED})
    status: NftOrderStatus;

    @Column({ name: 'created_at', type: 'bigint', nullable: false })
    public createdAt: number;

    @Column({ name: 'updated_at', type: 'bigint', nullable: false })
    public updatedAt: number;

    @BeforeInsert()
    public updateCreatedAt() {
        this.createdAt = nowInMillis();
        this.updatedAt = nowInMillis();
    }

    @BeforeUpdate()
    public updateUpdatedAt() {
        this.updatedAt = nowInMillis();
    }
}
